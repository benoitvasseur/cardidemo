//
//  CollisionViewController.m
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "CollisionViewController.h"

@interface CollisionViewController ()
@property (weak, nonatomic) IBOutlet UIView *redSquare;
@property (weak, nonatomic) IBOutlet UIView *blueSquare;

@property (nonatomic) CGRect redFrame;
@property (nonatomic) CGRect blueFrame;

@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UIGravityBehavior *gravityBehavior;
@property (nonatomic, strong) UICollisionBehavior *collisionBehavior;

@end

@implementation CollisionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.blueFrame = _blueSquare.frame;
    self.redFrame = _redSquare.frame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)resetSquares {
    _blueSquare.frame = _blueFrame;
    _redSquare.frame = _redFrame;
}

- (IBAction)startAnimation:(id)sender {
    [self resetSquares];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    _gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[_redSquare, _blueSquare]];
    _collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[_redSquare, _blueSquare]];
    _collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
    
    [_animator addBehavior:_collisionBehavior];
    [_animator addBehavior:_gravityBehavior];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
