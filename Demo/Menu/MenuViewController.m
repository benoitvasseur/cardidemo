//
//  MenuViewController.m
//  Demo
//
//  Created by Benoit on 09/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "MenuViewController.h"
#import "AppDelegate.h"
#import "DotsViewController.h"
#import "SpringAnimationViewController.h"
#import "ShapeDemoViewController.h"
#import "DynamicsViewController.h"
#import "VisualEffectViewController.h"
#import "ParallaxViewController.h"
#import "UIImageView+Animation.h"
#import "UILabel+Animation.h"

@interface MenuViewController ()
@property (nonatomic, strong) NSArray *titles;
@property (weak, nonatomic) IBOutlet UIImageView *arrowBottom;
@end

@implementation MenuViewController

- (void)viewDidLoad {
    self.titles = @[@"iOS 6", @"iOS 7", @"iOS 8"];
    
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeButtonPressed)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeGesture];
    
    
    // Pour nos différents pager
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor darkGrayColor];
    pageControl.backgroundColor = [UIColor whiteColor];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_arrowBottom animate];
    
    UIImageView *arrowTop = [[UIImageView alloc] initWithFrame:CGRectMake(_arrowBottom.frame.origin.x, self.view.frame.size.height - CGRectGetMaxY(_arrowBottom.frame), _arrowBottom.frame.size.width, _arrowBottom.frame.size.height)];
    arrowTop.image = [UIImage imageNamed:@"white_arrow.png"];
    arrowTop.transform = CGAffineTransformMakeRotation(M_PI);
    [self.view addSubview:arrowTop];
    [arrowTop animate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)closeButtonPressed {
    [APPDELEGATE openMenu];
}

#pragma mark - Scrollview delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat originalYOffset = self.view.frame.size.height;
    CGFloat originalXOffset = self.view.frame.size.width;
    CGFloat ratio = originalXOffset / originalYOffset;
    CGFloat newXOffset = MENU_WIDTH + abs(originalYOffset - scrollView.contentOffset.y) * ratio;
    UIViewController *centerVC = [APPDELEGATE centerViewController];
    [centerVC.view setFrame:CGRectMake(newXOffset, centerVC.view.frame.origin.y, centerVC.view.frame.size.width, centerVC.view.frame.size.height)];
}

#pragma mark - Swipe Menu

- (NSInteger)numberOfSection {
    return 3;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    } else if (section == 1) {
        return 3;
    } else if (section == 2) {
        return 2;
    }
    return 0;
}

- (NSString *)titleForSection:(NSInteger)section {
    return _titles[section];
}

- (UIButton *)buttonForRow:(NSInteger)row inSection:(NSInteger)section {
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    if (section == 0) {
        if (row == 0) {
            [button setTitle:@"Gestures" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(gesturePressed) forControlEvents:UIControlEventTouchUpInside];
        } else if (row == 1) {
            [button setTitle:@"Parallax" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(parallaxPressed) forControlEvents:UIControlEventTouchUpInside];
        }
    } else if (section == 1) {
        if (row == 0) {
            [button setTitle:@"CAShapeLayer" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(shapeLayerPressed) forControlEvents:UIControlEventTouchUpInside];
        } else if (row == 1) {
            [button setTitle:@"Spring Animation" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(springPressed) forControlEvents:UIControlEventTouchUpInside];
        } else if (row == 2) {
            [button setTitle:@"UIKit Dynamics" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(dynamicsPressed) forControlEvents:UIControlEventTouchUpInside];
        }
    } else if (section == 2) {
        if (row == 0) {
            [button setTitle:@"Visual Effect" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(VEPressed) forControlEvents:UIControlEventTouchUpInside];
        } else if (row == 1) {
            [button setTitle:@"Widget" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(widgetPressed) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    return button;
}

- (void)scrollViewDidScrollAtIndex:(NSInteger)index {
    UIViewController *vc = nil;
    switch (index) {
        case 0:
            vc = [[DotsViewController alloc] init];
            break;
        case 1:
            vc = [[UINavigationController alloc] initWithRootViewController:[[ShapeDemoViewController alloc] init]];
            break;
        case 2:
            vc = [[VisualEffectViewController alloc] initWithWavesArray:@[@[@"Attention voici la première vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"], @[@"Attention voici la seconde vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"], @[@"Attention voici la troisième vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"], @[@"Attention voici la quatrième vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"], @[@"Attention voici la cinquième vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"]]];
            break;
        default:
            break;
    }
    
    if (vc)
        [APPDELEGATE setCenterViewController:vc animated:YES];
}

#pragma mark - buttons

- (void)gesturePressed {
    UIViewController *vc = [[DotsViewController alloc] init];
    [self showViewController:vc];
}

- (void)parallaxPressed {
    UIViewController *vc = [[UINavigationController alloc] initWithRootViewController:[[ParallaxViewController alloc] init]];
    [self showViewController:vc];
}

- (void)shapeLayerPressed {
    UIViewController *vc = [[UINavigationController alloc] initWithRootViewController:[[ShapeDemoViewController alloc] init]];
    [self showViewController:vc];
}

- (void)springPressed {
    UIViewController *vc = [[UINavigationController alloc] initWithRootViewController:[[SpringAnimationViewController alloc] init]];
    [self showViewController:vc];
}

- (void)dynamicsPressed {
    UIViewController *vc = [[UINavigationController alloc] initWithRootViewController:[[DynamicsViewController alloc] init]];
    [self showViewController:vc];
}

- (void)VEPressed {
    UIViewController *vc = [[VisualEffectViewController alloc] initWithWavesArray:@[@[@"Attention voici la première vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"], @[@"Attention voici la seconde vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"], @[@"Attention voici la troisième vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"], @[@"Attention voici la quatrième vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"], @[@"Attention voici la cinquième vague", @"C'est chouette les springs animations", @"Ca permet de faire des animations vraiment sympas", @"Mais les visuals Effects sont pas mal aussi ;)", @"On rajoute quelques lignes !", @"Plus il y en a, plus c'est joli"]]];
    [self showViewController:vc];
}

- (void)widgetPressed {
    
}

- (void)showViewController:(UIViewController *)vc {
    if (vc) {
        [APPDELEGATE setCenterViewController:vc animated:NO];
        [APPDELEGATE closeMenu];
    }
}

@end
