//
//  SpringAnimationViewController.m
//  Demo
//
//  Created by Benoit on 09/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "SpringAnimationViewController.h"
#import "SquareAnimationViewController.h"
#import "PoPupViewController.h"
#import "AppDelegate.h"

@interface SpringAnimationViewController () <UIPageViewControllerDataSource>
@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) SquareAnimationViewController *squareController;
@property (nonatomic, strong) PoPupViewController *popupController;
@end

@implementation SpringAnimationViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor darkGrayColor];
    pageControl.backgroundColor = [UIColor whiteColor];
    
    self.title = @"Spring Animation";
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    _pageViewController.dataSource = self;
    _pageViewController.automaticallyAdjustsScrollViewInsets = NO;
    self.squareController = [[SquareAnimationViewController alloc] initWithNibName:nil bundle:nil];
    self.popupController = [[PoPupViewController alloc] initWithNibName:nil bundle:nil];
    
    [_pageViewController setViewControllers:@[_squareController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    UIBarButtonItem *openItem = [[UIBarButtonItem alloc] initWithTitle:@"Open" style:UIBarButtonItemStylePlain target:self action:@selector(openMenu)];
    self.navigationItem.leftBarButtonItem = openItem;
    
    
    // Permet d'utiliser les sliders sans déclencher la scrollview du pager
    for (UIScrollView *view in _pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.delaysContentTouches = NO;
        }
    }

}

- (void)openMenu {
    [APPDELEGATE openMenu];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [_pageViewController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Pager

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return 2;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    if (viewController == _squareController) {
        return _popupController;
    }
    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    if (viewController == _popupController) {
        return _squareController;
    }
    return nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
