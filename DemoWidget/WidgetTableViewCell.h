//
//  WidgetTableViewCell.h
//  Demo
//
//  Created by Benoit on 13/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WidgetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
