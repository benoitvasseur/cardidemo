//
//  AppDelegate.h
//  Demo
//
//  Created by Benoit on 09/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"

#define APPDELEGATE (AppDelegate *)((UIApplication *)[UIApplication sharedApplication]).delegate

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (UIPanGestureRecognizer *)panGesture;
- (void)openMenu;
- (void)closeMenu;
- (void)setCenterViewController:(UIViewController *)vc animated:(BOOL)animated;
- (UIViewController *)centerViewController;

@end

