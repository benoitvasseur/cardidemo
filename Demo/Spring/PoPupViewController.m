//
//  PoPupViewController.m
//  Demo
//
//  Created by Benoit on 09/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "PoPupViewController.h"

@interface PoPupViewController ()
@property (nonatomic, strong) UIView *popup;
@property (weak, nonatomic) IBOutlet UISlider *dampingValue;
@property (weak, nonatomic) IBOutlet UISlider *velocityValue;
@property (weak, nonatomic) IBOutlet UISlider *durationValue;
@property (weak, nonatomic) IBOutlet UILabel *dampingValuelabel;
@property (weak, nonatomic) IBOutlet UILabel *velocityValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationValueLabel;
@end

@implementation PoPupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showPopup:(id)sender {
    [self dismissPopup:nil];
    
    self.popup = [[UIView alloc] init];
    _popup.center = self.view.center;
    
    _popup.backgroundColor = [UIColor darkGrayColor];
    
    UILabel *popupLabel = [[UILabel alloc] init];
    popupLabel.font = [UIFont systemFontOfSize:13];
    popupLabel.text = @"Je suis une popup";
    popupLabel.textColor = [UIColor whiteColor];
    popupLabel.textAlignment = NSTextAlignmentCenter;
    [_popup addSubview:popupLabel];
    
    [self.view addSubview:_popup];
    
    // On lance l'animation spring, avec les valeurs définies
    [UIView animateWithDuration:_durationValue.value delay:0 usingSpringWithDamping:_dampingValue.value initialSpringVelocity:_velocityValue.value options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        _popup.frame = CGRectMake(0, 0, 200, 100);
        popupLabel.frame = _popup.frame;
        _popup.center = self.view.center;

    } completion:nil];
}

- (IBAction)dismissPopup:(id)sender {
    [_popup removeFromSuperview];
    self.popup = nil;
}


- (IBAction)sliderValueChanged:(id)sender {
    if (sender == _dampingValue) {
        [_dampingValuelabel setText:[NSString stringWithFormat:@"%.1f", _dampingValue.value]];
    } else if (sender == _velocityValue) {
        [_velocityValueLabel setText:[NSString stringWithFormat:@"%.1f", _velocityValue.value]];
    } else if (sender == _durationValue) {
        [_durationValueLabel setText:[NSString stringWithFormat:@"%.1f", _durationValue.value]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
