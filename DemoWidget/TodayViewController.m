//
//  TodayViewController.m
//  DemoWidget
//
//  Created by Benoit on 13/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "AFHTTPRequestOperationManager.h"
#import "WidgetTableViewCell.h"

static NSString *widgetCellId = @"widgetCellId";

@interface TodayViewController () <NCWidgetProviding, UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSDictionary *data;
@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableview registerNib:[UINib nibWithNibName:@"WidgetTableViewCell" bundle:nil] forCellReuseIdentifier:widgetCellId];
    
    [self getMusidcData:^(BOOL finished) {
        [self treatData];
    }];
    
    
    [self setPreferredContentSize:CGSizeMake(_tableview.frame.size.width, [self tableView:_tableview heightForRowAtIndexPath:nil] * [self tableView:_tableview numberOfRowsInSection:0] + 10)];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // Dispose of any resources that can be recreated.
}

// If implemented, the system will call at opportune times for the widget to update its state, both when the Notification Center is visible as well as in the background.
// An implementation is required to enable background updates.
// It's expected that the widget will perform the work to update asynchronously and off the main thread as much as possible.
// Widgets should call the argument block when the work is complete, passing the appropriate 'NCUpdateResult'.
// Widgets should NOT block returning from 'viewWillAppear:' on the results of this operation.
// Instead, widgets should load cached state in 'viewWillAppear:' in order to match the state of the view from the last 'viewWillDisappear:', then transition smoothly to the new data when it arrives.
- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    [self getMusidcData:^(BOOL finished){
        if (finished) {
            [self treatData];
            completionHandler(NCUpdateResultNewData);
        } else {
            completionHandler(NCUpdateResultFailed);
        }
    }];
}


- (void)treatData {
    NSString *artist = [_data objectForKey:@"artist"];
    NSString *title = [_data objectForKey:@"title"];
    NSString *newText = nil;
    if (artist && title) {
        newText = [NSString stringWithFormat:@"%@ %@", artist, title];
    } else if (artist || title) {
        newText = [NSString stringWithFormat:@"%@", artist ? artist : title];
    } else {
        newText = @"Aucun titre actuellement";
    }

    if (![newText isEqualToString:_text]) {
        self.text = newText;
        [self.tableview reloadData];
        [self setPreferredContentSize:CGSizeMake(_tableview.frame.size.width, [self tableView:_tableview heightForRowAtIndexPath:nil] * [self tableView:_tableview numberOfRowsInSection:0] + 10)];
    }
    
    /** On met à jour en continue les data
     ** La comme ça, j'aurais tendance à dire que c'est moche (on est censé laisser faire le "widgetPerformUpdateWithCompletionHandler")
     ** viewDidLoad est rappelé à chaque ouverture de la page Today, donc normalement on remet à jour le texte à chaque fois
     ** Mais bon, c'est une démo :) !
     */
    [self getMusidcData:^(BOOL finished) {
        [self treatData];
    }];
}

- (void)getMusidcData:(void (^)(BOOL didUpdate))completion {
    // On récupère les données du morceau jouer sur OUI FM
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://indesradios.int.cardiweb.com/services/pull?rid=2174546520932614531&mid=206h1knyk31&version=1" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        self.data = [responseObject objectForKey:@"data"];
        completion(YES);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        completion(NO);
    }];
}

#pragma mark - tableview

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
    NSURL *url = [NSURL URLWithString:@"lesindesradios://murduson?id=2174546520932614531"];
    [self.extensionContext openURL:url completionHandler:nil];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WidgetTableViewCell *cell = (WidgetTableViewCell *)[tableView dequeueReusableCellWithIdentifier:widgetCellId];

    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.minimumScaleFactor = 0.3;
    if (_text) {
        cell.label.text = _text;
    } else {
        // On bluff ! Mais idéalement ici il faudrait du cache (mais c'est naze pour le cas des indés ...)
        cell.label.text = @"Aucun titre actuellement";
    }
    return cell;
}


@end
