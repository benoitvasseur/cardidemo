//
//  VisualEffectViewController.h
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeViewController.h"

@interface VisualEffectViewController : SwipeViewController

/* Tableau de tableau de NSString*
** Chaque sous tableau correspond à une vague de labels
*/
@property (nonatomic, strong) NSArray *wavesArray;

- (id)initWithWavesArray:(NSArray *)wavesArray;

@end
