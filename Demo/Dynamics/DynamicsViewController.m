//
//  DynamicsViewController.m
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "DynamicsViewController.h"
#import "AppDelegate.h"
#import "CollisionViewController.h"
#import "AttachementViewController.h"
#import "SnapViewController.h"

@interface DynamicsViewController () <UIPageViewControllerDataSource>
@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) CollisionViewController *collisionViewController;
@property (nonatomic, strong) AttachementViewController *attachementViewController;
@property (nonatomic, strong) SnapViewController *snapViewController;
@end

@implementation DynamicsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Spring Animation";
    
    self.collisionViewController = [[CollisionViewController alloc] initWithNibName:nil bundle:nil];
    self.attachementViewController = [[AttachementViewController alloc] initWithNibName:nil bundle:nil];
    self.snapViewController = [[SnapViewController alloc] initWithNibName:nil bundle:nil];
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    _pageViewController.dataSource = self;
    _pageViewController.automaticallyAdjustsScrollViewInsets = NO;
    
    [_pageViewController setViewControllers:@[_collisionViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    UIBarButtonItem *openItem = [[UIBarButtonItem alloc] initWithTitle:@"Open" style:UIBarButtonItemStylePlain target:self action:@selector(openMenu)];
    self.navigationItem.leftBarButtonItem = openItem;
    
    // Permet d'utiliser les sliders sans déclencher la scrollview du pager
    for (UIScrollView *view in _pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.delaysContentTouches = NO;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [_pageViewController didMoveToParentViewController:self];
}

- (void)openMenu {
    [APPDELEGATE openMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Pager

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return 3;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    if (viewController == _collisionViewController) {
        return _attachementViewController;
    } else if (viewController == _attachementViewController) {
        return _snapViewController;
    }
    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    if (viewController == _attachementViewController) {
        return _collisionViewController;
    } else if (viewController == _snapViewController) {
        return _attachementViewController;
    }
    return nil;
}


@end
