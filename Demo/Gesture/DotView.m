//
//  DotView.m
//  Scrollview
//
//  Created by Benoit on 24/09/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "DotView.h"

#define ARC4RANDOM_MAX      0x100000000
#define MAX_SIZE            80
#define MIN_SIZE            15
#define NEAT_ROW_NUMBER     4

@interface DotView ()
@property (nonatomic, strong) UIColor *switchColor;
@end

@implementation DotView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (void)arrangeDotsRandomlyInView:(UIView *)view {
    float maxX = view.bounds.size.width;
    float maxY = view.bounds.size.height;
    
    for (UIView *subview in view.subviews) {
        if ([subview isKindOfClass:[DotView class]]) {
            subview.frame = CGRectMake(arc4random_uniform(maxX - subview.frame.size.width), arc4random_uniform(maxY - subview.frame.size.height), subview.frame.size.width, subview.frame.size.height);
        }
    }
}

+ (void)arrangeDotsNeatlyInView:(UIView *)view {
    float currentX = view.frame.size.width / (2 * NEAT_ROW_NUMBER);
    float currentY = currentX;
    float increment = view.frame.size.width / NEAT_ROW_NUMBER;
    float counter = 0;
    
    for (UIView *subview in view.subviews) {
        if ([subview isKindOfClass:[DotView class]]) {
            if (counter >= NEAT_ROW_NUMBER) {
                counter = 0;
                currentY += increment;
                currentX = view.frame.size.width / (2 * NEAT_ROW_NUMBER);
            }
            subview.center = CGPointMake(currentX, currentY);
            ++counter;
            currentX += increment;
        }
    }
}

+ (void)arrangeDotsNeatlyInView:(UIView *)view withAnimation:(BOOL)withAnimation {
    if (withAnimation) {
        [UIView animateWithDuration:0.3 animations:^{
            [self arrangeDotsNeatlyInView:view];
        }];
    } else {
        [self arrangeDotsNeatlyInView:view];
    }
}

+ (DotView *)randomDotView {
    int size = 0;
    while (size < MIN_SIZE) {
        size = arc4random_uniform(MAX_SIZE);
    }
    DotView *dotview = [[DotView alloc] initWithFrame:CGRectMake(0, 0, size, size)];
    dotview.layer.cornerRadius = size / 2;
    float red = ((float)arc4random() / ARC4RANDOM_MAX);
    float blue = ((float)arc4random() / ARC4RANDOM_MAX);
    float green = ((float)arc4random() / ARC4RANDOM_MAX);
    
    dotview.backgroundColor = [UIColor colorWithRed:red green:green blue:blue alpha:1];
    dotview.switchColor = [UIColor colorWithRed:red green:green blue:blue alpha:0.5];
    return dotview;
}

- (void)setHightlighted:(BOOL)shouldHighlight {
    if (shouldHighlight) {
        self.alpha = 0.8;
    } else {
        self.alpha = 1;
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self setHightlighted:YES];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self setHightlighted:NO];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self setHightlighted:NO];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    // On gère les petits points
    CGRect touchBounds = self.bounds;
    if (self.frame.size.width <= 40) {
        CGFloat expansion = (40 - self.frame.size.width) / 2;
        touchBounds = CGRectInset(touchBounds, -expansion, -expansion);
    }
    return CGRectContainsPoint(touchBounds, point);
}

@end
