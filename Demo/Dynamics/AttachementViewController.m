//
//  AttachementViewController.m
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "AttachementViewController.h"

@interface AttachementViewController ()
@property (weak, nonatomic) IBOutlet UIView *redSquare;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (nonatomic) CGRect redFrame;

@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UIGravityBehavior *gravityBehavior;
@property (nonatomic, strong) UIAttachmentBehavior *attachementBehavior;
@end

@implementation AttachementViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _redFrame = _redSquare.frame;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)resetSquares {
    _redSquare.frame = _redFrame;
}

- (IBAction)startAnimation:(id)sender {
    [self resetSquares];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    _gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[_redSquare]];
    _attachementBehavior = [[UIAttachmentBehavior alloc] initWithItem:_redSquare attachedToAnchor:self.view.center];
    _attachementBehavior.length = 100;
    
    [_animator addBehavior:_attachementBehavior];
    [_animator addBehavior:_gravityBehavior];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
