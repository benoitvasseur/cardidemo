//
//  AppDelegate.m
//  Demo
//
//  Created by Benoit on 09/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import "MEZoomAnimationController.h"
#import "MainViewController.h"

@interface AppDelegate ()
@property (nonatomic, strong) ECSlidingViewController *sideMenuViewController;
@property (nonatomic, strong) MenuViewController *menuViewController;
@property (nonatomic, strong) MainViewController *mainViewController;
@property (nonatomic, strong) MEZoomAnimationController *animationController;
@property (nonatomic) BOOL menuIsOpen;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    self.menuViewController = [[MenuViewController alloc] initWithNibName:nil bundle:nil];
    self.mainViewController = [[MainViewController alloc] initWithNibName:nil bundle:nil];
    
    self.sideMenuViewController = [ECSlidingViewController slidingWithTopViewController:[[UINavigationController alloc] initWithRootViewController:self.mainViewController]];
    self.sideMenuViewController.underLeftViewController  = _menuViewController;
    self.animationController = [[MEZoomAnimationController alloc] init];
    self.sideMenuViewController.anchorRightRevealAmount = MENU_WIDTH;
    self.sideMenuViewController.delegate = _animationController;
    
    self.window.rootViewController = self.sideMenuViewController;
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;

}

- (UIPanGestureRecognizer *)panGesture {
    return self.sideMenuViewController.panGesture;
}

- (void)openMenu {
    if (_menuIsOpen) {
        [self closeMenu];
    } else {
        [self.sideMenuViewController anchorTopViewToRightAnimated:YES];
        _menuIsOpen = YES;
    }
}

- (void)closeMenu {
    [self.sideMenuViewController resetTopViewAnimated:YES];
    _menuIsOpen = NO;
}

- (UIViewController *)centerViewController {
    return self.sideMenuViewController.topViewController;
}

- (void)setCenterViewController:(UIViewController *)vc animated:(BOOL)animated {
    if (animated) {
        CGRect containerViewFrame = self.sideMenuViewController.topViewController.view.frame;
        self.sideMenuViewController.topViewController.view.frame = CGRectMake(0, self.sideMenuViewController.topViewController.view.frame.origin.y, self.sideMenuViewController.topViewController.view.frame.size.width, self.sideMenuViewController.topViewController.view.frame.size.height);
        self.sideMenuViewController.anchorRightRevealAmount = MENU_WIDTH;
        [self.sideMenuViewController setTopViewController:vc];
        
        self.sideMenuViewController.topViewController.view.frame = containerViewFrame;
        [UIView animateWithDuration:0.2 animations:^{
            self.sideMenuViewController.topViewController.view.frame = CGRectMake(0, self.sideMenuViewController.topViewController.view.frame.origin.y, self.sideMenuViewController.topViewController.view.frame.size.width, self.sideMenuViewController.topViewController.view.frame.size.height);
        }];
//        self.sideMenuViewController.anchorRightPeekAmount = 0;
//        __block CGRect containerViewFrame = self.sideMenuViewController.topViewController.view.frame;
//        const CGRect containerViewFrameConst = containerViewFrame;
//        [UIView animateWithDuration:0.2 animations:^{
//            containerViewFrame.origin.x = self.sideMenuViewController.anchorRightRevealAmount;
//            self.sideMenuViewController.topViewController.view.frame = containerViewFrame;
//        } completion:^(BOOL finished) {
//             /* Ici on réaffecte l'ancienne frame et anchorRight afin de placer le viewController
//             ** au bon endroit pour après l'animation (si ces infos sont mauvaises, il s'embrouille durant
//             ** durant le setTopViewController:), et ensuite on fait simplement l'animation en animant la frame
//             */
//            self.sideMenuViewController.topViewController.view.frame = containerViewFrameConst;
//            self.sideMenuViewController.anchorRightRevealAmount = MENU_WIDTH;
//            [self.sideMenuViewController setTopViewController:vc];
//            
//            self.sideMenuViewController.topViewController.view.frame = containerViewFrame;
//            [UIView animateWithDuration:0.2 animations:^{
//               self.sideMenuViewController.topViewController.view.frame = containerViewFrameConst;
//            }];
//        }];
    } else {
        [self.sideMenuViewController setTopViewController:vc];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
