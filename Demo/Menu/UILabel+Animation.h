//
//  UILabel+Animation.h
//  Demo
//
//  Created by Benoit on 29/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Animation)
- (void)setTextWithChangeAnimation:(NSString*)text;
@end
