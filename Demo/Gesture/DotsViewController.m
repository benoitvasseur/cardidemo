//
//  ViewController.m
//  Scrollview
//
//  Created by Benoit on 24/09/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "DotsViewController.h"
#import "DotView.h"
#import "OverlayScrollView.h"
#import "TouchDelayGestureRecognizer.h"

@interface DotsViewController () <UIGestureRecognizerDelegate>
@property (nonatomic, weak, readonly) UIView *drawerContentView;
@end

@implementation DotsViewController {
    UIView *_canvasView;
    UIScrollView *_scrollView;
    UIVisualEffectView *_drawerView;
}

- (UIView *)drawerContentView {
    return [_drawerView isKindOfClass:[UIVisualEffectView class]] ? _drawerView.contentView : _drawerView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect bounds = self.view.bounds;
    
    _canvasView = [[UIView alloc] initWithFrame:bounds];
    _canvasView.backgroundColor = [UIColor darkGrayColor];
    [_canvasView setUserInteractionEnabled:YES];
    [self.view addSubview:_canvasView];
    
    // Permet d'éviter l'activation du dot quand on commence à scroller dessus
    TouchDelayGestureRecognizer *touchDelay = [[TouchDelayGestureRecognizer alloc] initWithTarget:nil action:nil];
    [_canvasView addGestureRecognizer:touchDelay];
    //-----
    
    [self addDots:20 toView:_canvasView];
    [DotView arrangeDotsRandomlyInView:_canvasView];
    
    _scrollView = [[OverlayScrollView alloc] initWithFrame:bounds];
    [self.view addSubview:_scrollView];
    
    _drawerView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    if (!_drawerView) { // iOS < 8
        _drawerView = (UIVisualEffectView *)[[UIView alloc] init]; // Oui on bluff, mais j'aime pas les warnings :p
        _drawerView.alpha = 0.9;
        _drawerView.backgroundColor = [UIColor blackColor];
    }
    _drawerView.frame = CGRectMake(0, 0, bounds.size.width, 290);
    [_scrollView addSubview:_drawerView];
    
    [self addDots:11 toView:self.drawerContentView];
    [DotView arrangeDotsNeatlyInView:self.drawerContentView withAnimation:NO];
    
    _scrollView.contentSize = CGSizeMake(bounds.size.width, bounds.size.height + _drawerView.frame.size.height);
    _scrollView.contentOffset = CGPointMake(0, _drawerView.frame.size.height);
    [self.view addGestureRecognizer:_scrollView.panGestureRecognizer];
}

- (void)addDots:(NSUInteger)count toView:(UIView *)view {
    for (NSUInteger i = 0; i < count; ++i) {
        DotView *dotView = [DotView randomDotView];
        [view addSubview:dotView];
        
        UILongPressGestureRecognizer *longpress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        longpress.cancelsTouchesInView = NO;
        longpress.delegate = self;
        [dotView addGestureRecognizer:longpress];
    }
    
    [DotView arrangeDotsNeatlyInView:self.drawerContentView withAnimation:YES];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gesture {
    UIView *dot = gesture.view;
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            [self grabDot:dot withGesture:gesture];
            break;
        case UIGestureRecognizerStateChanged:
            [self moveDot:dot withGesture:gesture];
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
            [self dropDot:dot withGesture:gesture];
        default:
            break;
    }
}

- (void)grabDot:(UIView *)dot withGesture:(UILongPressGestureRecognizer *)gesture {
    dot.center = [self.view convertPoint:dot.center fromView:dot.superview];
    [self.view addSubview:dot];
    
    [UIView animateWithDuration:0.2 animations:^{
        dot.transform = CGAffineTransformMakeScale(1.2, 1.2);
        dot.alpha = 0.8;
        [self moveDot:dot withGesture:gesture];
    }];
    
    // De cette facon déplacer un dot ne fait pas bouger la scrollview
    _scrollView.panGestureRecognizer.enabled = NO;
    _scrollView.panGestureRecognizer.enabled = YES;
    
    [DotView arrangeDotsNeatlyInView:self.drawerContentView withAnimation:YES];
}

- (void)moveDot:(UIView *)dot withGesture:(UILongPressGestureRecognizer *)gesture {
    dot.center = [gesture locationInView:self.view];
}

- (void)dropDot:(UIView *)dot withGesture:(UILongPressGestureRecognizer *)gesture {
    [UIView animateWithDuration:0.2 animations:^{
        dot.transform = CGAffineTransformIdentity;
        dot.alpha = 1;
    }];
    
    CGPoint locationInDrawer = [gesture locationInView:_drawerView];
    if (CGRectContainsPoint([_drawerView bounds], locationInDrawer)) {
        [self.drawerContentView addSubview:dot];
    } else {
        [_canvasView addSubview:dot];
    }
    
    dot.center = [self.view convertPoint:dot.center fromView:dot.superview];
    
    [DotView arrangeDotsNeatlyInView:self.drawerContentView withAnimation:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)recognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
