//
//  ImageViewController.m
//  Demo
//
//  Created by Benoit on 31/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()
@end

@implementation ImageViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // L'image est deux fois plus large que la taille de l'écran
    [_imageView setImage:[UIImage imageNamed:_imageName]];
}

@end
