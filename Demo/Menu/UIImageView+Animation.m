//
//  UIImageView+Animation.m
//  Demo
//
//  Created by Benoit on 29/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "UIImageView+Animation.h"

@implementation UIImageView (Animation)


- (void)animate {
    CALayer *maskLayer = [CALayer layer];
    
    // Mask image ends with 0.15 opacity on both sides. Set the background color of the layer
    // to the same value so the layer can extend the mask image.
    maskLayer.backgroundColor = [[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.15f] CGColor];
    maskLayer.contents = (id)[[UIImage imageNamed:@"MaskVertical.png"] CGImage];
    
    // Center the mask image on twice the width of the text layer, so it starts to the left
    // of the text layer and moves to its right when we translate it by width.
    maskLayer.contentsGravity = kCAGravityCenter;
    maskLayer.frame = CGRectMake(0, self.frame.size.height * -1, self.frame.size.width, self.frame.size.height * 2);
    
    // Animate the mask layer's horizontal position
    CABasicAnimation *maskAnim = [CABasicAnimation animationWithKeyPath:@"position.y"];
    maskAnim.byValue = [NSNumber numberWithFloat:self.frame.size.height];
    maskAnim.repeatCount = FLT_MAX;
    maskAnim.duration = 2.0f;
    [maskLayer addAnimation:maskAnim forKey:@"slideAnim"];
    
    self.layer.mask = maskLayer;
}

@end
