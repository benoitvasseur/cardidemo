//
//  ParallaxViewController.m
//  Demo
//
//  Created by Benoit on 31/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "ParallaxViewController.h"
#import "ImageViewController.h"
#import "AppDelegate.h"

@interface ParallaxViewController () <UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate>
@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) ImageViewController *viewController1;
@property (nonatomic, strong) ImageViewController *viewController2;
@property (nonatomic, strong) ImageViewController *viewController3;

@property (nonatomic, strong) ImageViewController *otherVC;
@property (nonatomic) BOOL draggindDidEnd;
@end

@implementation ParallaxViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Parallax";
    
    self.viewController1 = [[ImageViewController alloc] initWithNibName:nil bundle:nil];
    [_viewController1 setImageName:@"parallax1.jpg"];
    _viewController1.index = 1;
    self.viewController2 = [[ImageViewController alloc] initWithNibName:nil bundle:nil];
    [_viewController2 setImageName:@"parallax2.jpg"];
    _viewController2.index = 2;
    self.viewController3 = [[ImageViewController alloc] initWithNibName:nil bundle:nil];
    [_viewController3 setImageName:@"parallax3.jpg"];
    _viewController3.index = 3;
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    _pageViewController.dataSource = self;
    _pageViewController.delegate = self;
    _pageViewController.automaticallyAdjustsScrollViewInsets = NO;
    
    [_pageViewController setViewControllers:@[_viewController1] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    UIBarButtonItem *openItem = [[UIBarButtonItem alloc] initWithTitle:@"Open" style:UIBarButtonItemStylePlain target:self action:@selector(openMenu)];
    self.navigationItem.leftBarButtonItem = openItem;
    
    // On récupère la scrollview afin de pouvoir être notifié quand le pager scroll (et donc pouvoir animer les images)
    for (UIView *view in self.pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            [(UIScrollView *)view setDelegate:self];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [_pageViewController didMoveToParentViewController:self];
}

- (void)openMenu {
    [APPDELEGATE openMenu];
}

#pragma mark - Scrollview

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    ImageViewController *currentViewController = [_pageViewController.viewControllers firstObject];
    if (_draggindDidEnd)
        return;

    // Au repos l'offset est égal à la largeur de l'écran
    if (scrollView.contentOffset.x > self.view.frame.size.width) {
        // Vers la droite
        ImageViewController *nextVC = (ImageViewController *)[self pageViewController:_pageViewController viewControllerAfterViewController:currentViewController];
        [nextVC.imageView setFrame:CGRectMake(- nextVC.imageView.frame.size.width / 2 // Correspond à l'offset de base quand on va vers la droite
                                              + (scrollView.contentOffset.x - self.view.frame.size.width) / 2, 0, nextVC.imageView.frame.size.width, nextVC.imageView.frame.size.height)];
    } else if (scrollView.contentOffset.x < self.view.frame.size.width) {
        // Vers la gauche
        ImageViewController *prevVC = (ImageViewController *)[self pageViewController:_pageViewController viewControllerBeforeViewController:currentViewController];
        // Vers la gauche, l'offset de base est à 0
        [prevVC.imageView setFrame:CGRectMake((scrollView.contentOffset.x - self.view.frame.size.width) / 2, 0, prevVC.imageView.frame.size.width, prevVC.imageView.frame.size.height)];
    }
    if (scrollView.contentOffset.x != self.view.frame.size.width) {
        [currentViewController.imageView setFrame:CGRectMake((- self.view.frame.size.width / 2) // L'image est centrée (et est deux fois plus large que l'écran)
                                                             + (scrollView.contentOffset.x - self.view.frame.size.width) / 2, 0, currentViewController.imageView.frame.size.width, currentViewController.imageView.frame.size.height)];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    NSLog(@"offset : %@", NSStringFromCGPoint(scrollView.contentOffset));
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    // On recentre l'image
    ImageViewController *currentViewController = [_pageViewController.viewControllers firstObject];
    
    if (scrollView.contentOffset.x > self.view.frame.size.width * 3 / 2
        || scrollView.contentOffset.x < self.view.frame.size.width / 2) {
        // On change d'écran
        // On fait le nouvel écran immédiatement
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            [_otherVC.imageView setFrame:CGRectMake((- self.view.frame.size.width / 2), 0, currentViewController.imageView.frame.size.width, currentViewController.imageView.frame.size.height)];
        } completion:nil];
        // On fait l'ancien un peu plus tard
        [UIView animateWithDuration:0 delay:0.3 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
                [currentViewController.imageView setFrame:CGRectMake((- self.view.frame.size.width / 2), 0, currentViewController.imageView.frame.size.width, currentViewController.imageView.frame.size.height)];
        } completion:nil];
    } else {
        // On reste sur le même / Ou alors on change à cause d'un scroll rapide
        // On fait le nouveau maintenant, car on change peut-être d'écran ...
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            [_otherVC.imageView setFrame:CGRectMake((- self.view.frame.size.width / 2), 0, currentViewController.imageView.frame.size.width, currentViewController.imageView.frame.size.height)];
        } completion:nil];
        // On fait l'ancien (qui reste à l'écran, ou pas) maintenant
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            [currentViewController.imageView setFrame:CGRectMake((- self.view.frame.size.width / 2), 0, currentViewController.imageView.frame.size.width, currentViewController.imageView.frame.size.height)];
        } completion:nil];
    }
    _draggindDidEnd = YES;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    _draggindDidEnd = NO;
}

#pragma mark - Pager

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers {
    self.otherVC = [pendingViewControllers firstObject];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {

}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return 3;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    if (viewController == _viewController1) {
        return _viewController2;
    } else if (viewController == _viewController2) {
        return _viewController3;
    }
    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    if (viewController == _viewController2) {
        return _viewController1;
    } else if (viewController == _viewController3) {
        return _viewController2;
    }
    return nil;
}


@end
