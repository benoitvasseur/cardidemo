//
//  SquareAnimationViewController.m
//  Demo
//
//  Created by Benoit on 09/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "SquareAnimationViewController.h"

@interface SquareAnimationViewController ()
@property (weak, nonatomic) IBOutlet UIView *redSquare;
@property (weak, nonatomic) IBOutlet UIView *blueSquare;
@property (weak, nonatomic) IBOutlet UILabel *springLabel;
@property (weak, nonatomic) IBOutlet UILabel *easeLabel;

@end

@implementation SquareAnimationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)startAnimation:(id)sender {
    CGFloat targetY = _springLabel.frame.origin.y;
    
    [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.15 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        CGRect blueFrame = _blueSquare.frame;
        blueFrame.origin.y = targetY - blueFrame.size.height - 10;
        _blueSquare.frame = blueFrame;
    } completion:nil];
    
    [UIView animateWithDuration:1 animations:^{
        CGRect redFrame = _redSquare.frame;
        redFrame.origin.y = targetY - redFrame.size.height - 10;
        _redSquare.frame = redFrame;
    }];
}

- (IBAction)resetTouched:(id)sender {
    CGRect blueFrame = _blueSquare.frame;
    blueFrame.origin.y = 97;
    _blueSquare.frame = blueFrame;
    
    CGRect redFrame = _redSquare.frame;
    redFrame.origin.y = 97;
    _redSquare.frame = redFrame;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
