//
//  VisualEffectViewController.m
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "VisualEffectViewController.h"
#import "UILabel+Animation.h"

#define SHOW_DURATION 3
#define VELOCITY 1
#define LABEL_HEIGHT 20
#define DAMPING 1

@interface VisualEffectViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) NSArray *labelsShown;
@property (nonatomic, strong) NSArray *labelsToHide;
@property (nonatomic, strong) UIView *titleContainer;
@end

@implementation VisualEffectViewController

- (id)initWithWavesArray:(NSArray *)wavesArray {
    if (self = [super initWithNibName:nil bundle:nil]) {
        self.wavesArray = wavesArray;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *backgroundEffect = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    backgroundEffect.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    backgroundEffect.frame = self.view.frame;
    [self.view addSubview:backgroundEffect];
    self.view.clipsToBounds = YES;
    
    
    // On donne un effet au label
    UIVisualEffect *vibrancyEffect = [UIVibrancyEffect effectForBlurEffect:(UIBlurEffect *)backgroundEffect.effect];
    UIVisualEffectView *vibrancyLabelContainerView = [[UIVisualEffectView alloc] initWithEffect:vibrancyEffect];
    vibrancyLabelContainerView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    vibrancyLabelContainerView.frame = _titleLabel.frame;
    _titleLabel.frame = vibrancyLabelContainerView.bounds;
    [vibrancyLabelContainerView.contentView addSubview:_titleLabel];
    [backgroundEffect.contentView addSubview:vibrancyLabelContainerView];
    self.titleContainer = vibrancyLabelContainerView;
    
    [_titleLabel setTextWithChangeAnimation:@"Visual Effect"];
    
    [self.view bringSubviewToFront:_titleLabel];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self performSelector:@selector(startAnimating) withObject:nil afterDelay:2];
}

- (void)startAnimating {
    [self showWaveNumber:[NSNumber numberWithInt:0]];
}

- (void)showWaveNumber:(NSNumber *)waveNumber {
    if ([NSThread currentThread] != [NSThread mainThread]) {
        return [self performSelectorOnMainThread:_cmd withObject:nil waitUntilDone:NO];
    }
    
    int waveInt = [waveNumber intValue];
    NSArray *waveStrings = _wavesArray[waveInt];
    self.labelsToHide = _labelsShown;
    NSMutableArray *newLabels = [[NSMutableArray alloc] initWithCapacity:waveStrings.count];
    
    CGFloat space = ((self.view.bounds.size.height - _titleContainer.frame.origin.y - 100) - (waveStrings.count + 1) * LABEL_HEIGHT) / (waveStrings.count + 1);
    
    CGFloat offset = self.view.bounds.size.height + _titleContainer.frame.origin.y + 100;
    
    // On crée les labels à partir des strings
    for (NSString *string in waveStrings) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, offset, self.view.bounds.size.width, 20)];
        label.text = string;
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont systemFontOfSize:17];
        label.textAlignment = NSTextAlignmentCenter;
        label.adjustsFontSizeToFitWidth = YES;
        label.minimumScaleFactor = 0.3;
        [newLabels addObject:label];
        offset += space;
        [self.view addSubview:label];
    }
    
    CGFloat delay = 0;
    for (UILabel *label in newLabels) {
        [UIView animateWithDuration:SHOW_DURATION delay:delay usingSpringWithDamping:DAMPING initialSpringVelocity:VELOCITY options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            // On entre
            CGRect labelFrame = label.frame;
            labelFrame.origin.y -= self.view.frame.size.height;
            label.frame = labelFrame;
        } completion:^(BOOL finished){
            // On sort
            [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:DAMPING initialSpringVelocity:VELOCITY options:UIViewAnimationOptionAllowAnimatedContent animations:^{
                CGRect labelFrame = label.frame;
                labelFrame.origin.y = CGRectGetMinY(_titleContainer.frame); // On fait disparaitre au niveau de titre
                label.frame = labelFrame;
                label.alpha = 0;
            } completion:nil];
        }];
        delay += 0.1;
    }
    
    self.labelsShown = newLabels;
    
    // On incrémente le numéro de la vague
    NSNumber *nextWaveNumber = waveInt + 1 < _wavesArray.count ? [NSNumber numberWithInt:++waveInt] : [NSNumber numberWithInt:0];
    
    [self performSelector:@selector(showWaveNumber:) withObject:nextWaveNumber afterDelay:SHOW_DURATION + 0.3];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
