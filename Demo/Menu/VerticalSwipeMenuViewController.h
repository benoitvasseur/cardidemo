//
//  VerticalSwipeMenuViewController.h
//  Demo
//
//  Created by Benoit on 14/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GBInfiniteScrollView/GBInfiniteScrollView.h>

#define MENU_WIDTH 200

@protocol VerticalSwipeMenuViewControllerDelegate <NSObject>
- (NSInteger)numberOfSection;
- (NSInteger)numberOfRowsInSection:(NSInteger)section;
- (UIButton *)buttonForRow:(NSInteger)row inSection:(NSInteger)section;

@optional
- (UIColor *)titleColorForSection:(NSInteger)section;
- (NSString *)titleForSection:(NSInteger)section;
- (CGFloat)widthForSection:(NSInteger)section;
- (void)scrollViewDidScrollAtIndex:(NSInteger)index;
@end

@interface VerticalSwipeMenuViewController : UIViewController <VerticalSwipeMenuViewControllerDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) GBInfiniteScrollView *infiniteScrollView;

@end
