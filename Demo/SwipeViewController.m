//
//  SwipeViewController.m
//  Demo
//
//  Created by Benoit on 09/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "SwipeViewController.h"
#import "AppDelegate.h"


@interface SwipeViewController ()
@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;
@end

@implementation SwipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem *openItem = [[UIBarButtonItem alloc] initWithTitle:@"Open" style:UIBarButtonItemStylePlain target:self action:@selector(openMenu)];
    self.navigationItem.leftBarButtonItem = openItem;
    
    [self.view addGestureRecognizer:[APPDELEGATE panGesture]];
    
    // Do any additional setup after loading the view.
}

- (void)openMenu {
    [APPDELEGATE openMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
