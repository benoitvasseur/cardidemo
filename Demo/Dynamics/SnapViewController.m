//
//  SnapViewController.m
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "SnapViewController.h"

@interface SnapViewController () <UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIView *redSquare;

@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UISnapBehavior *snapBehavior;
@end

@implementation SnapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILongPressGestureRecognizer *longpress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longpress.cancelsTouchesInView = NO;
    longpress.delegate = self;
    [_redSquare addGestureRecognizer:longpress];
    
    _redSquare.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - LongPress

- (void)handleLongPress:(UILongPressGestureRecognizer *)gesture {
    UIView *square = gesture.view;
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            [self grabSquare:square withGesture:gesture];
            break;
        case UIGestureRecognizerStateChanged:
            [self moveSquare:square withGesture:gesture];
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
            [self dropSquare:square withGesture:gesture];
        default:
            break;
    }
}

- (void)grabSquare:(UIView *)square withGesture:(UILongPressGestureRecognizer *)gesture {
    square.center = [self.view convertPoint:square.center fromView:square.superview];
    
    [UIView animateWithDuration:0.2 animations:^{
        [self moveSquare:square withGesture:gesture];
    }];
}

- (void)moveSquare:(UIView *)square withGesture:(UILongPressGestureRecognizer *)gesture {
    square.center = [gesture locationInView:self.view];
}

- (void)dropSquare:(UIView *)square withGesture:(UILongPressGestureRecognizer *)gesture {
    [self moveSquare:square withGesture:gesture];
    
    _animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    _snapBehavior = [[UISnapBehavior alloc] initWithItem:_redSquare snapToPoint:self.view.center];
    
    [_animator addBehavior:_snapBehavior];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
