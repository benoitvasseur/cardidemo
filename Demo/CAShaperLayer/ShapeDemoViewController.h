//
//  ShapeDemoViewController.h
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeViewController.h"

@interface ShapeDemoViewController : SwipeViewController

@end
