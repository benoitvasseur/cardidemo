//
//  LoaderShapeView.h
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoaderShapeView : UIView

@property (nonatomic, strong) CAShapeLayer *gradientMask;

- (void)setStrokeEnd:(CGFloat)strokeEnd;
- (CGFloat)getStrokeEnd;

@end
