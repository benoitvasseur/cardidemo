//
//  ImageViewController.h
//  Demo
//
//  Created by Benoit on 31/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController

@property (strong, nonatomic) NSString *imageName;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) NSInteger index;


@end
