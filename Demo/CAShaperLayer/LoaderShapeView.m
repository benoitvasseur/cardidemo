//
//  LoaderShapeView.m
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "LoaderShapeView.h"

@interface LoaderShapeView ()

@end

@implementation LoaderShapeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)awakeFromNib {

    // On crée un bezier path circulaire, on va essayer de reproduire le loader de l'AppStore
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath addArcWithCenter:CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2) radius:(self.bounds.size.height - 10) / 2 startAngle:-M_PI_2 endAngle:3.0 * M_PI_2 clockwise:YES];
 
    CAShapeLayer *gradientMask = [CAShapeLayer layer];
    gradientMask.fillColor = [[UIColor clearColor] CGColor];
    gradientMask.strokeColor = [[UIColor blackColor] CGColor];
    gradientMask.lineWidth = 10;
    [gradientMask setLineJoin:kCALineJoinRound];
    [gradientMask setLineCap:kCALineCapRound];
    gradientMask.frame = CGRectMake(0, 0, self.bounds.size.width - 10, self.bounds.size.height - 10);
    
    gradientMask.path = bezierPath.CGPath;
    
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.startPoint = CGPointMake(0.5,1.0);
    gradientLayer.endPoint = CGPointMake(0.5,0.0);
    gradientLayer.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    NSMutableArray *colors = [NSMutableArray array];
    for (int i = 0; i < 10; i++) {
        [colors addObject:(id)[[UIColor colorWithHue:(0.1 * i) saturation:1 brightness:.8 alpha:1] CGColor]];
    }
    gradientLayer.colors = colors;
    
    [gradientLayer setMask:gradientMask];
    [self.layer addSublayer:gradientLayer];
    
    self.gradientMask = gradientMask;
}

//  StrokeEnd correspond à l'espace non affiché de la fin du layer (du path en l'occurence)
- (void)setStrokeEnd:(CGFloat)strokeEnd {
    [_gradientMask setStrokeEnd:strokeEnd];
}

- (CGFloat)getStrokeEnd {
    return [_gradientMask strokeEnd];
}

@end
