//
//  ViewController.h
//  Scrollview
//
//  Created by Benoit on 24/09/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SwipeViewController.h"

@interface DotsViewController : SwipeViewController

@end
