//
//  ShapeDemoViewController.m
//  Demo
//
//  Created by Benoit on 10/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "ShapeDemoViewController.h"
#import "LoaderShapeView.h"

@interface ShapeDemoViewController ()
@property (weak, nonatomic) IBOutlet LoaderShapeView *loaderShape;
@property (weak, nonatomic) IBOutlet UILabel *avancementLabel;
@property (weak, nonatomic) IBOutlet UISlider *avancementValue;
@property (weak, nonatomic) IBOutlet UIImageView *loadingImage;
@end

@implementation ShapeDemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sliderValueChanged:(id)sender {
    _loadingImage.hidden = YES;
    _avancementLabel.text = [NSString stringWithFormat:@"%.1f",  _avancementValue.value];
    [_loaderShape setStrokeEnd:_avancementValue.value];
}


- (IBAction)animateLoader:(id)sender {
    _loadingImage.image = [UIImage imageNamed:@"stop"];
    _loadingImage.hidden = NO;
    
    CABasicAnimation *stroke = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    stroke.fromValue = @(0.0);
    stroke.toValue = @(1.0);
    stroke.duration = 2.0f;
    [_loaderShape.gradientMask addAnimation:stroke forKey:nil];
    
    _avancementValue.userInteractionEnabled = NO;
    
    [self performSelector:@selector(endAnimation) withObject:nil afterDelay:2];
}

- (void)endAnimation {
    if ([NSThread currentThread] != [NSThread mainThread]) {
        return [self performSelectorOnMainThread:_cmd withObject:nil waitUntilDone:NO];
    }
    _loadingImage.image = [UIImage imageNamed:@"play"];
    
    _avancementValue.userInteractionEnabled = YES;
    _avancementValue.value = [_loaderShape getStrokeEnd];
    _avancementLabel.text = [NSString stringWithFormat:@"%.1f", [_loaderShape getStrokeEnd]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
