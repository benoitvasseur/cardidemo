//
//  VerticalSwipeMenuViewController.m
//  Demo
//
//  Created by Benoit on 14/10/2014.
//  Copyright (c) 2014 Cardiweb. All rights reserved.
//

#import "VerticalSwipeMenuViewController.h"

@interface VerticalSwipeMenuViewController () <GBInfiniteScrollViewDataSource, GBInfiniteScrollViewDelegate>
@property (nonatomic) BOOL isFirstLoad;
@end

@implementation VerticalSwipeMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isFirstLoad = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!_infiniteScrollView) {
        GBInfiniteScrollView *infiniteScrollView = [[GBInfiniteScrollView alloc] initWithFrame:CGRectMake(0, 0, MENU_WIDTH, self.view.bounds.size.height)];
        
        infiniteScrollView.infiniteScrollViewDataSource = self;
        infiniteScrollView.infiniteScrollViewDelegate = self;
        
        infiniteScrollView.pageIndex = 0;
        infiniteScrollView.scrollDirection = GBScrollDirectionVertical;
        [infiniteScrollView setDebug:YES];
        infiniteScrollView.delegate = self;
        [self.view addSubview:infiniteScrollView];
        
        [infiniteScrollView reloadData];
        self.infiniteScrollView = infiniteScrollView;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - A surcharger

- (CGFloat)widthForSection:(NSInteger)section {
    return 200;
}


- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (NSInteger)numberOfSection {
    return 0;
}

- (NSString *)titleForSection:(NSInteger)section {
    return @"";
}

- (UIColor *)titleColorForSection:(NSInteger)section {
    return [UIColor blackColor];
}

- (UIButton *)buttonForRow:(NSInteger)row inSection:(NSInteger)section {
    return nil;
}

#pragma mark - infinite Scrollview

- (void)infiniteScrollViewDidScrollNextPage:(GBInfiniteScrollView *)infiniteScrollView
{
    if ([self respondsToSelector:@selector(scrollViewDidScrollAtIndex:)] && !_isFirstLoad)
        [self scrollViewDidScrollAtIndex:infiniteScrollView.currentPageIndex];
    NSLog(@"Next page");
}

- (void)infiniteScrollViewDidScrollPreviousPage:(GBInfiniteScrollView *)infiniteScrollView
{
    if ([self respondsToSelector:@selector(scrollViewDidScrollAtIndex:)] && !_isFirstLoad)
        [self scrollViewDidScrollAtIndex:infiniteScrollView.currentPageIndex];
    NSLog(@"Previous page");
}

- (BOOL)infiniteScrollViewShouldScrollNextPage:(GBInfiniteScrollView *)infiniteScrollView
{
    return YES;
}

- (BOOL)infiniteScrollViewShouldScrollPreviousPage:(GBInfiniteScrollView *)infiniteScrollView
{
    return YES;
}

- (NSInteger)numberOfPagesInInfiniteScrollView:(GBInfiniteScrollView *)infiniteScrollView
{
    return [self numberOfSection];
}

- (GBInfiniteScrollViewPage *)infiniteScrollView:(GBInfiniteScrollView *)infiniteScrollView pageAtIndex:(NSUInteger)index;
{
    UILabel *titleSection = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [self widthForSection:index], 20)];
    titleSection.textColor = [self titleColorForSection:index];
    titleSection.backgroundColor = [UIColor clearColor];
    titleSection.text = [self titleForSection:index];
    titleSection.textAlignment = NSTextAlignmentCenter;
    
    UIView *section = [[UIView alloc] initWithFrame:CGRectMake((infiniteScrollView.frame.size.width - [self widthForSection:index]) / 2, 0, [self widthForSection:index], titleSection.frame.size.height)];

    [section addSubview:titleSection];
    for (int j = 0; j < [self numberOfRowsInSection:index]; ++j) {
        // On construit un bouton, et on agrandit la vue
        UIButton *button = [self buttonForRow:j inSection:index];
        [button setFrame:CGRectMake(0, section.frame.size.height, section.frame.size.width, button.frame.size.height)];
        [section setFrame:CGRectMake(section.frame.origin.x, (self.view.bounds.size.height - section.frame.size.height - button.frame.size.height) / 2, section.frame.size.width, section.frame.size.height + button.frame.size.height)];
        [section addSubview:button];
    }
    
    section.tag = 1;
    
    GBInfiniteScrollViewPage *page = [infiniteScrollView dequeueReusablePage];
    [[page viewWithTag:1] removeFromSuperview];
    
    if (page == nil) {
        page = [[GBInfiniteScrollViewPage alloc] initWithFrame:self.view.bounds style:GBInfiniteScrollViewPageStyleText];
    }
    [page.contentView addSubview:section];
    
    if (_isFirstLoad)
        _isFirstLoad = NO;
    
    return page;
}

@end
